

CREATE TABLE `author` (
  `authorId` int NOT NULL,
  `Author` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`authorId`)
) 


CREATE TABLE `book_detail` (
  `bookId` int NOT NULL,
  `Title` varchar(20) DEFAULT NULL,
  `ISBN` varchar(13) DEFAULT NULL,
  `Num_copies` int DEFAULT NULL,
  `authID` int DEFAULT NULL,
  `publishID` int DEFAULT NULL,
  PRIMARY KEY (`bookId`),
  CONSTRAINT `book_detail_ibfk_1` FOREIGN KEY (`authID`) REFERENCES `author` (`authorId`),
  CONSTRAINT `book_detail_ibfk_2` FOREIGN KEY (`publishID`) REFERENCES `publisher` (`publisherId`)
) 


CREATE TABLE `branch_detail` (
  `Branch` int NOT NULL,
  `Branch_Addr` varchar(100) DEFAULT NULL,
  `bookDetailID` int DEFAULT NULL,
  PRIMARY KEY (`Branch`),
  CONSTRAINT `branch_detail_ibfk_1` FOREIGN KEY (`bookDetailID`) REFERENCES `book_detail` (`bookId`)
) 


CREATE TABLE `publisher` (
  `publisherId` int NOT NULL,
  `Publisher` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`publisherId`)
) 