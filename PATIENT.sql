
CREATE TABLE `PATIENT_TABLE` (
  `Patient` int NOT NULL,
  `PatientName` varchar(20) NOT NULL,
  `PatientDOB` date DEFAULT NULL,
  `PatientAddressID` INT DEFAULT NULL,
  PRIMARY KEY (`Patient`),
  CONSTRAINT `FK_PatientAddressID` FOREIGN KEY (`PatientAddressID`) REFERENCES `ADDRESS_TABLE` (`AddressId`),
) 

CREATE TABLE `ADDRESS_TABLE` (
  `AddressId` int NOT NULL,
  `AddressArea` varchar(50) NOT NULL,
  PRIMARY KEY (`AddressId`)
) 



CREATE TABLE `PRESCRIPTION_TABLE` (
  `Prescription` int NOT NULL,
  `DrugID` INT DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `Dosage` int DEFAULT NULL,
  `PatientId` int DEFAULT NULL,
  `DoctorId` int DEFAULT NULL,
  PRIMARY KEY (`Prescription`),
  CONSTRAINT `FK_DoctorId` FOREIGN KEY (`DoctorId`) REFERENCES `DOCTOR_TABLE` (`Doctor`),
  CONSTRAINT `FK_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `PATIENT_TABLE` (`Patient`),
  CONSTRAINT `FK_DrugID` FOREIGN KEY (`DrugID`) REFERENCES `DRUG_TABLE` (`DrugId`),
) 

CREATE TABLE `DRUG_TABLE` (
  `DrugId` INT NOT NULL,
  `DrugName` varchar(100) NOT NULL,
  PRIMARY KEY (`DrugId`)
) 

CREATE TABLE `DOCTOR_TABLE` (
  `Doctor` int NOT NULL,
  `DoctorName` varchar(20) NOT NULL,
  `SecretaryID` INT DEFAULT NULL,
  PRIMARY KEY (`Doctor`),
  CONSTRAINT `FK_SecretaryID` FOREIGN KEY (`SecretaryID`) REFERENCES `SECRETARY_TABLE` (`SecretaryId`),
) 

CREATE TABLE `SECRETARY_TABLE` (
  `SecretaryId` int NOT NULL,
  `SecretaryName` varchar(20) NOT NULL,
  PRIMARY KEY (`SecretaryId`)
) 
