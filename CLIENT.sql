
CREATE TABLE `CLIENT_TABLE` (
  `Client` int NOT NULL,
  `Name` varchar(20) NOT NULL,
  `LocationID` INT DEFAULT NULL,
  `Client_contract` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`Client`),
  CONSTRAINT `FK_ClientId` FOREIGN KEY (`Client_contract`) REFERENCES `CONTRACT_TABLE` (`Contract`),
  CONSTRAINT `FK_locationId` FOREIGN KEY (`LocationID`) REFERENCES `LOCATION_TABLE` (`LocationId`)
) 


CREATE TABLE `CONTRACT_TABLE` (
  `Contract` varchar(10) NOT NULL,
  `Estimated_cost` float DEFAULT NULL,
  `Completion_date` date DEFAULT NULL,
  PRIMARY KEY (`Contract`),
  UNIQUE KEY `Contract` (`Contract`)
) 

CREATE TABLE `MANAGER_TABLE` (
  `Manager` int NOT NULL,
  `Manager_name` varchar(20) NOT NULL,
  `LocationID` INT DEFAULT NULL,
  `Manager_contract` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`Manager`),
  CONSTRAINT `FK_managerId` FOREIGN KEY (`Manager_contract`) REFERENCES `CONTRACT_TABLE` (`Contract`),
  CONSTRAINT `FK_locationId` FOREIGN KEY (`LocationID`) REFERENCES `LOCATION_TABLE` (`LocationId`)
) 


CREATE TABLE `STAFF_TABLE` (
  `Staff` int NOT NULL,
  `Staff_name` varchar(20) NOT NULL,
  `LocationID` INT DEFAULT NULL,
  `Staff_contract` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`Staff`),
  CONSTRAINT `FK_StaffId` FOREIGN KEY (`Staff_contract`) REFERENCES `CONTRACT_TABLE` (`Contract`),
  CONSTRAINT `FK_locationId` FOREIGN KEY (`LocationID`) REFERENCES `LOCATION_TABLE` (`LocationId`)
) 

CREATE TABLE `LOCATION_TABLE` (
  `LocationId` int NOT NULL,
  `Location` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`LocationId`)
  }

